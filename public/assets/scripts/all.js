"use strict";

var _cards = require("modules/cards.js");

window.onload = function () {
  card_container = document.getElementById("card-container");

  var strings = ["The world will end in fire. All will burn.", "Die. Die. Die.", "What is going on?", "Must have more.", "Can't stop the power."];
  card_manager = new _cards.CardManager(card_container);

  card_manager.addCard(_cards.WhiteCard, "Make me an offer I can't refuse. _");
  card_manager.addCard(_cards.WhiteCard, "Help! My _ has been replaced with _.");

  for (var i = 0; i < strings.length; i++) {
    card_manager.addCard(_cards.BlackCard, strings[i]);
  }Resize();

  window.addEventListener("resize", Resize);
}; //Nothing for now
// socket  = io();
// cards   = require('./modules/cards.js');

function Resize(e) {
  console.log("HI");
  var content = document.getElementById("content");
  var nav = document.getElementById("nav");

  if (true) {
    content.style.height = window.innerHeight - 50 + "px";
    content.style.top = "50px;";
    nav.style.height = "50px";
  } else {
    content.style.height = window.innerHeight + "px";
    content.style.top = "0px;";
    nav.style.height = "0px";
  }
}
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Card = exports.Card = function () {
  function Card() {
    _classCallCheck(this, Card);
  }

  _createClass(Card, [{
    key: "addTo",
    value: function addTo(elem) {
      elem.appendChild(this.element);
      return this;
    }
  }, {
    key: "remove",
    value: function remove() {
      this.element.parentElement.removeChild(this.element);
    }
  }]);

  return Card;
}();

var BlackCard = exports.BlackCard = function (_Card) {
  _inherits(BlackCard, _Card);

  function BlackCard(str) {
    _classCallCheck(this, BlackCard);

    var _this = _possibleConstructorReturn(this, (BlackCard.__proto__ || Object.getPrototypeOf(BlackCard)).call(this));

    _this.element = document.createElement("div");
    _this.inner_div = _this.element.appendChild(document.createElement("div"));
    _this.text = _this.inner_div.appendChild(document.createTextNode(str));
    _this.controls = _this.inner_div.appendChild(document.createElement("div"));
    _this.up = _this.controls.appendChild(document.createElement("div"));
    _this.num = _this.controls.appendChild(document.createElement("div"));
    _this.down = _this.controls.appendChild(document.createElement("div"));

    _this.up.innerHTML = '<i class="fa fa-chevron-up" aria-hidden="true"></i>';
    _this.down.innerHTML = '<i class="fa fa-chevron-down" aria-hidden="true"></i>';
    _this.num.innerHTML = '0';

    _this.element.className = "black-card";
    _this.inner_div.className = "card-inner";
    _this.controls.className = "black-card-controls";
    _this.up.className = "black-card-up";
    _this.num.className = "black-card-num";
    _this.down.className = "black-card-down";
    return _this;
  }

  _createClass(BlackCard, [{
    key: "getText",
    value: function getText() {
      return this.text.textContent;
    }
  }, {
    key: "setText",
    value: function setText(str) {
      this.text.textContent = str;
    }
  }, {
    key: "getNum",
    value: function getNum() {
      return parseInt(this.num.innerHTML);
    }
  }, {
    key: "setNum",
    value: function setNum(num) {
      window.setTimeout(this.animateNum.bind(this), 50);

      this.num.innerHTML = num;
    }
  }, {
    key: "animateNum",
    value: function animateNum() {
      if (this.num.style.transform === "") this.num.style.transform = "rotate3d(1,0,0,360deg)";else this.num.style.transform = "";
    }
  }, {
    key: "click",
    value: function click(e) {
      if (e.target.className.indexOf("up") != -1) {
        this.setNum(this.getNum() + 1);
        e.preventDefault();
      }

      if (e.target.className.indexOf("down") != -1) {
        this.setNum(this.getNum() - 1);
        e.preventDefault();
      }

      if (e.target.className.indexOf("num") != -1) {
        confirm("Do you really want to choose: " + this.getText());
      }

      return this;
    }
  }]);

  return BlackCard;
}(Card);

var WhiteCard = exports.WhiteCard = function (_Card2) {
  _inherits(WhiteCard, _Card2);

  function WhiteCard(str) {
    _classCallCheck(this, WhiteCard);

    var _this2 = _possibleConstructorReturn(this, (WhiteCard.__proto__ || Object.getPrototypeOf(WhiteCard)).call(this));

    _this2.element = document.createElement("div");
    _this2.inner_div = _this2.element.appendChild(document.createElement("div"));
    _this2.text = _this2.inner_div.appendChild(document.createTextNode(str.replace(/_/g, "_____")));
    // this.controls   = this.inner_div.appendChild(document.createElement("div"));
    // this.up         = this.controls.appendChild(document.createElement("div"));
    // this.num        = this.controls.appendChild(document.createElement("div"));
    // this.down       = this.controls.appendChild(document.createElement("div"));
    //
    // this.up.innerHTML   = '<i class="fa fa-chevron-up" aria-hidden="true"></i>';
    // this.down.innerHTML = '<i class="fa fa-chevron-down" aria-hidden="true"></i>';
    // this.num.innerHTML  = '0';

    _this2.element.className = "white-card";
    _this2.inner_div.className = "card-inner";
    // this.controls.className   = "black-card-controls";
    // this.up.className         = "black-card-up";
    // this.num.className        = "black-card-num";
    // this.down.className       = "black-card-down";
    return _this2;
  }

  return WhiteCard;
}(Card);

var CardManager = exports.CardManager = function () {
  function CardManager(container) {
    _classCallCheck(this, CardManager);

    this.index_id = 0;
    this.deck = [];
    this.container = container;
    this.container.addEventListener("mouseup", this.click.bind(this));
    // this.container.addEventListener("touchend", this.click.bind(this));
  }

  _createClass(CardManager, [{
    key: "click",
    value: function click(e) {
      for (var i = 0; i < this.deck.length; i++) {
        if (this.deck[i].element.contains(e.target)) {
          if (this.deck[i].click !== undefined) this.deck[i].click(e);

          this.sort();

          break;
        }
      }
    }
  }, {
    key: "addCard",
    value: function addCard(type, str) {
      return this.deck[this.deck.push(new type(str, this.index_id++)) - 1].addTo(this.container);
    }
  }, {
    key: "removeCard",
    value: function removeCard(card) {
      for (var i = 0; i < this.deck.length; i++) {
        if (this.deck[i].id == card.id) {
          this.deck.splice(i, 1);
          return;
        }
      }
    }
  }, {
    key: "sort",
    value: function sort() {
      for (var i = 0; i < this.deck.length; i++) {
        this.deck[i].remove();
      }

      this.deck.sort(function (a, b) {
        if (!a.num) return -1;

        if (!b.num) return 1;

        return -Math.sign(parseInt(a.num.innerHTML) - parseInt(b.num.innerHTML));
      });

      for (var i = 0; i < this.deck.length; i++) {
        this.deck[i].addTo(this.container);
      }
    }
  }]);

  return CardManager;
}();
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _cards = require('./modules/cards.js');

//Nothing for now
// console.log(window.location.href);

var socket;

//Variables
var name = '';
var pass = '';
var card_container;
var card_manager;
var state;
var id;
var whites = [];
var blacks = [];

window.onload = function () {
  socket = new io();
  //Activate login prompts

  if (!localStorage['name']) {
    localStorage['name'] = window.prompt('Enter your name:');
    localStorage['pass'] = window.prompt('Enter your password:');
  }

  // TODO Stop sending plaintext password

  socket.on('ready-auth', SendAuth);
  socket.on('auth-result', AuthResult);
  socket.on('new-state', NewState);
};

function SendAuth() {
  console.log("Authenticating");
  socket.emit('auth', {
    name: localStorage['name'],
    pass: localStorage['pass'],
    id: socket.id
  });
}

function AuthResult(data) {
  console.log(data.state);

  state = data.state;
  id = data.player_id;

  if (data.auth) {
    GenerateCards();
  } else {
    card_container = document.getElementById("card-container");
    card_manager = new _cards.CardManager(card_container, Action);

    card_manager.addCard(_cards.WhiteCard, "Sorry you entered invalid credentials, refresh to try again!");
    delete localStorage['name'];
    delete localStorage['pass'];
  }

  Resize();
  window.addEventListener("resize", Resize);
}

function Action(action_obj) {
  // console.log(action_obj);
  socket.emit('action', {
    socket_id: socket.id,
    text: action_obj.text,
    type: action_obj.type
  });
}

function NewState(data) {
  state = data;

  GenerateCards();
}

function GenerateCards() {
  if (card_container != undefined) {
    card_container.innerHTML = '';
  } else {
    card_container = document.getElementById("card-container");
  }

  card_manager = new _cards.CardManager(card_container, Action);

  switch (state.players[id].mode) {
    case 'choosing-white':
      card_manager.addCard(_cards.LastRoundCard, state.last_round, state.czar);
      card_manager.addCard(_cards.PlayersCard, state.players, state.czar);
      card_manager.addCard(_cards.BlackCard, state.black);
      card_manager.addCard(_cards.InfoCard, "Submit a card!");
      for (var i = 0; i < state.players[id].whites.length; i++) {
        card_manager.addCard(_cards.WhiteCard, state.players[id].whites[i]);
      }break;

    case 'choosing-winner':
      card_manager.addCard(_cards.LastRoundCard, state.last_round, state.czar);
      card_manager.addCard(_cards.PlayersCard, state.players, state.czar);
      card_manager.addCard(_cards.BlackCard, state.black);
      card_manager.addCard(_cards.InfoCard, "You are choosing a winner!");
      for (var i = 0; i < state.options.length; i++) {
        card_manager.addCard(_cards.WhiteCard, state.options[i]);
      }break;

    case 'waiting-players':
      card_manager.addCard(_cards.LastRoundCard, state.last_round, state.czar);
      card_manager.addCard(_cards.PlayersCard, state.players, state.czar);
      card_manager.addCard(_cards.BlackCard, state.black);
      card_manager.addCard(_cards.InfoCard, "Waiting for other players to submit");
      break;

    case 'waiting-czar':
      card_manager.addCard(_cards.LastRoundCard, state.last_round, state.czar);
      card_manager.addCard(_cards.PlayersCard, state.players, state.czar);
      card_manager.addCard(_cards.BlackCard, state.black);
      card_manager.addCard(_cards.InfoCard, "Waiting for the czar to choose");
      break;
  }
}

function Resize(e) {
  var content = document.getElementById("content");
  var nav = document.getElementById("nav");

  if (true) {
    content.style.height = window.innerHeight - 50 + "px";
    content.style.top = "50px;";
    nav.style.height = "50px";
  } else {
    content.style.height = window.innerHeight + "px";
    content.style.top = "0px;";
    nav.style.height = "0px";
  }
}

},{"./modules/cards.js":2}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Card = exports.Card = function () {
  function Card(manager) {
    _classCallCheck(this, Card);

    this.manager = manager;
    this.id = manager.index_id++;
  }

  _createClass(Card, [{
    key: 'addTo',
    value: function addTo(elem) {
      elem.appendChild(this.element);
      return this;
    }
  }, {
    key: 'remove',
    value: function remove() {
      this.element.parentElement.removeChild(this.element);
    }
  }, {
    key: 'getNum',
    value: function getNum() {
      return parseInt(this.element.getAttribute('sort-num'));
    }
  }, {
    key: 'setNum',
    value: function setNum(num) {
      this.element.setAttribute('sort-num', num);
    }
  }]);

  return Card;
}();

var WhiteCard = exports.WhiteCard = function (_Card) {
  _inherits(WhiteCard, _Card);

  function WhiteCard(manager, args) {
    _classCallCheck(this, WhiteCard);

    var _this = _possibleConstructorReturn(this, (WhiteCard.__proto__ || Object.getPrototypeOf(WhiteCard)).call(this, manager));

    _this.str = args[0];

    _this.element = document.createElement("div");
    _this.inner_div = _this.element.appendChild(document.createElement("div"));
    _this.text = _this.inner_div.appendChild(document.createElement("span"));
    _this.text.innerHTML = _this.str;

    _this.controls = _this.inner_div.appendChild(document.createElement("div"));
    _this.up = _this.controls.appendChild(document.createElement("div"));
    _this.num = _this.controls.appendChild(document.createElement("div"));
    _this.down = _this.controls.appendChild(document.createElement("div"));

    _this.up.innerHTML = '<i class="fa fa-chevron-up" aria-hidden="true"></i>';
    _this.down.innerHTML = '<i class="fa fa-chevron-down" aria-hidden="true"></i>';
    _this.num.innerHTML = '0';

    _this.element.className = "white-card";
    _this.inner_div.className = "card-inner";
    _this.controls.className = "white-card-controls";
    _this.up.className = "white-card-up";
    _this.num.className = "white-card-num";
    _this.down.className = "white-card-down";

    _this.setNum(0);
    return _this;
  }

  _createClass(WhiteCard, [{
    key: 'getText',
    value: function getText() {
      return this.str;
    }
  }, {
    key: 'setText',
    value: function setText(str) {
      this.str = str;
      this.text.innerHTML = str;
    }
  }, {
    key: 'setNum',
    value: function setNum(num) {
      var _this2 = this;

      _get(WhiteCard.prototype.__proto__ || Object.getPrototypeOf(WhiteCard.prototype), 'setNum', this).call(this, num);

      window.setTimeout(function () {
        return _this2.animateNum();
      }, 50);

      this.num.innerHTML = num;
    }
  }, {
    key: 'animateNum',
    value: function animateNum() {
      if (this.num.style.transform === "") this.num.style.transform = "rotate3d(1,0,0,360deg)";else this.num.style.transform = "";
    }
  }, {
    key: 'click',
    value: function click(e) {
      if (e.target.className.indexOf("up") != -1) {
        this.setNum(this.getNum() + 1);
        e.preventDefault();
      }

      if (e.target.className.indexOf("down") != -1) {
        this.setNum(this.getNum() - 1);
        e.preventDefault();
      }

      if (e.target.className.indexOf("num") != -1) {
        if (confirm("Do you really want to choose: " + this.getText())) {
          this.manager.action({
            card: this,
            type: 'submit-white',
            text: this.getText()
          });
        }
      }

      return this;
    }
  }]);

  return WhiteCard;
}(Card);

var BlackCard = exports.BlackCard = function (_Card2) {
  _inherits(BlackCard, _Card2);

  function BlackCard(manager, args) {
    _classCallCheck(this, BlackCard);

    var _this3 = _possibleConstructorReturn(this, (BlackCard.__proto__ || Object.getPrototypeOf(BlackCard)).call(this, manager));

    var str = args[0];

    _this3.element = document.createElement("div");
    _this3.inner_div = _this3.element.appendChild(document.createElement("div"));
    _this3.text = _this3.inner_div.appendChild(document.createElement("span"));
    _this3.text.innerHTML = str.replace(/_/g, "_____");

    _this3.element.className = "black-card";
    _this3.inner_div.className = "card-inner";

    _this3.setNum(1000);
    return _this3;
  }

  return BlackCard;
}(Card);

var PlayersCard = exports.PlayersCard = function (_Card3) {
  _inherits(PlayersCard, _Card3);

  function PlayersCard(manager, args) {
    _classCallCheck(this, PlayersCard);

    var _this4 = _possibleConstructorReturn(this, (PlayersCard.__proto__ || Object.getPrototypeOf(PlayersCard)).call(this, manager));

    var player_arr = args[0];
    var czar_id = args[1];

    _this4.element = document.createElement("div");
    _this4.inner_div = _this4.element.appendChild(document.createElement("div"));

    _this4.players = [];

    for (var i = 0, len = player_arr.length; i < len; i++) {
      _this4.players[i] = _this4.inner_div.appendChild(document.createElement("div"));
      _this4.players[i].innerHTML = (i == czar_id ? '<i class="fa fa-hand-rock-o" aria-hidden="true"></i> ' : '<i class="fa fa-hand-rock-o invisible" aria-hidden="true"></i> ') + player_arr[i].name + "<span class='name-right'>[" + player_arr[i].score + "](" + (player_arr[i].submitted ? '<i class="fa fa-check" aria-hidden="true"></i> ' : '<i class="fa fa-check invisible" aria-hidden="true"></i> ') + ")</span>";
    }

    _this4.element.className = "special-card";
    _this4.inner_div.className = "card-inner";

    _this4.setNum(2000);
    return _this4;
  }

  _createClass(PlayersCard, [{
    key: 'fixLen',
    value: function fixLen(str, num) {
      while (str.length < num) {
        str += " ";
      }return str;
    }
  }]);

  return PlayersCard;
}(Card);

var LastRoundCard = exports.LastRoundCard = function (_Card4) {
  _inherits(LastRoundCard, _Card4);

  function LastRoundCard(manager, args) {
    _classCallCheck(this, LastRoundCard);

    var _this5 = _possibleConstructorReturn(this, (LastRoundCard.__proto__ || Object.getPrototypeOf(LastRoundCard)).call(this, manager));

    var last_result = args[0];

    _this5.element = document.createElement("div");
    _this5.inner_div = _this5.element.appendChild(document.createElement("div"));

    _this5.last_black = _this5.inner_div.appendChild(document.createElement("div"));
    _this5.last_white = _this5.inner_div.appendChild(document.createElement("div"));

    _this5.last_black.innerHTML = last_result.black.replace(/_/g, "_____");
    _this5.last_white.innerHTML = last_result.white;

    _this5.element.className = "special-card";
    _this5.inner_div.className = "card-inner";
    _this5.last_black.className = 'last-black';
    _this5.last_white.className = 'last-white';

    _this5.setNum(3000);
    return _this5;
  }

  return LastRoundCard;
}(Card);

var InfoCard = exports.InfoCard = function (_Card5) {
  _inherits(InfoCard, _Card5);

  function InfoCard(manager, args) {
    _classCallCheck(this, InfoCard);

    var _this6 = _possibleConstructorReturn(this, (InfoCard.__proto__ || Object.getPrototypeOf(InfoCard)).call(this, manager));

    var text = args[0];

    _this6.element = document.createElement("div");
    _this6.inner_div = _this6.element.appendChild(document.createElement("div"));

    _this6.inner_div.innerHTML = text;

    _this6.element.className = "special-card";
    _this6.inner_div.className = "card-inner";

    _this6.setNum(100);
    return _this6;
  }

  return InfoCard;
}(Card);

var CardManager = exports.CardManager = function () {
  function CardManager(container, action) {
    _classCallCheck(this, CardManager);

    this.index_id = 0;
    this.deck = [];
    console.log(container);
    this.container = container;
    this.container.addEventListener("mouseup", this.click.bind(this));
    this.action = action;
    // this.container.addEventListener("touchend", this.click.bind(this));
  }

  _createClass(CardManager, [{
    key: 'click',
    value: function click(e) {
      for (var i = 0; i < this.deck.length; i++) {
        if (this.deck[i].element.contains(e.target)) {
          if (this.deck[i].click !== undefined) this.deck[i].click(e);

          this.sort();

          break;
        }
      }
    }
  }, {
    key: 'addCard',
    value: function addCard(type) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      return this.deck[this.deck.push(new type(this, args)) - 1].addTo(this.container);
    }
  }, {
    key: 'removeCard',
    value: function removeCard(card) {
      for (var i = 0; i < this.deck.length; i++) {
        if (this.deck[i].id == card.id) {
          this.deck.splice(i, 1);
          return;
        }
      }
    }
  }, {
    key: 'sort',
    value: function sort() {
      for (var i = 0; i < this.deck.length; i++) {
        this.deck[i].remove();
      }

      this.deck.sort(function (a, b) {
        return -(a.getNum() - b.getNum());
      });

      for (var i = 0; i < this.deck.length; i++) {
        this.deck[i].addTo(this.container);
      }
    }
  }]);

  return CardManager;
}();

},{}]},{},[1]);

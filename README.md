# SamCAH
---

## Openshift git mergeing
rhc app create APPNAME diy-0.1 http://cartreflect-claytondev.rhcloud.com/reflect?github=smarterclayton/openshift-redis-cart
cd APPNAME
git remote add github -f https://samcarlin@bitbucket.org/samcarlin/samcah.git
git merge github/master -s recursive -X theirs --allow-unrelated-histories
git push origin master


## Dependencies
  * Nunjucks
    * Tutorial: http://zellwk.com/blog/nunjucks-with-gulp/
    * Pages folder for actual html documents, templates and partials for reusable bits.
  * Sass
    * Information stolen from scotch.io \*shrug\*
  * Browserify
    * Modules found in modules folder
  * monitorctrlc
    * Suppresses terminate batch job prompt in windows

"use strict";
// New Code
// const mongo   = require('mongodb');
// const monk    = require('monk');
// const db      = monk('localhost:27017/samcah');
// const users   = db.get("users");

class CardDataManager {
  constructor() {
    this.unused_black = cards["Base"].black.slice();
    this.unused_white = cards["Base"].white.slice();

    this.used_black = [];
    this.used_white = [];

    for(var i=0; i<this.unused_black.length; i++) {
      if(cards.blackCards[this.unused_black[i]].pick != 1) {
        this.unused_black.splice(i, 1);
        i--;
      }
    }

    this.original_black = this.unused_black.slice();
    this.original_white = this.unused_white.slice();

    this.unused_black = this.shuffle(this.unused_black);
    this.unsued_white = this.shuffle(this.unused_white);
  }

  getBlackCard() {
    var text = cards.blackCards[this.unused_black.splice(0, 1)[0]].text;

    if(this.unused_black.length == 0) {
      this.unused_black = this.original_black.slice();
      this.unused_black = this.shuffle(this.unused_black);
    }

    return text;
  }

  getWhiteCards(num) {
    var text = [];

    while(num > 0) {
      text.push(cards.whiteCards[this.unused_white.splice(0, 1)[0]]);

      if(this.unused_white.length == 0) {
        this.unused_white = this.original_white.slice();
        this.unused_white = this.shuffle(this.unused_white);
      }

      num--;
    }

    return text;
  }

  shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      let index = Math.floor(Math.random() * counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      let temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }

    return array;
  }
}

class StateStorage {
  constructor(client) {
    this.client = client || {};
    this.current_version = 0.61;
  }

  getState(callback) {
    this.callback = callback

    if(!this.client.get) {
      this.callback(this.generateDefault());

      return;
    }

    this.client.get('state', (err, result) => this.getComplete(err, result));
  }

  getComplete(err, results) {
    var state = JSON.parse(results);

    if(!state || state.app_version < this.current_version) {
      this.setState(this.generateDefault());

      this.callback(this.generateDefault());

      return;
    }

    this.callback(state);
  }

  setState(state) {
    if(!this.client.set)
      return;

    this.local_state = Object.assign({}, state);

    this.client.set('state', JSON.stringify(this.local_state), (err, result) => this.setComplete(err, result));
  }

  setComplete(err, results) {
    //Nothing doing
  }

  generateDefault() {
    return {
      app_version: this.current_version,
      players: this.generatePlayerObj(['Sam', 'themaster', 'Sasha', 'angry_orchard', 'Jimmy', 'red_turtle', 'Aidan', 'global_goal', 'Mark', 'jumping_junebugs', 'Jacob', 'robot_overlord']),
      czar: 0,
      black: card_data_manager.getBlackCard(),
      whites: [],
      options: [],
      last_round: {
        black: 'Looks like this is round 1',
        white: 'Hai :)'
      }
    }
  }

  generatePlayerObj(arr) {
    var objs = [];

    for(var i=0,len=arr.length; i<len; i+=2) {
      objs.push({
        name: arr[i],
        pass: arr[i+1],
        score: 0,
        whites: card_data_manager.getWhiteCards(10),
        submitted: undefined,
        mode: i == 0 ? 'waiting-players' : 'choosing-white'
      })
    }

    return objs;
  }
}

class Room {
  constructor() {
    this.state = {};

    this.waiting = [];
    this.sockets = [];

    this.loadState();
  }

  loadState() {
    state_storage.getState(state => this.stateLoaded(state));
  }

  stateLoaded(state) {
    console.log(this.waiting);

    this.state = state;
  }

  connect(socket) {
    console.log("Connection!" + socket.id);
    this.waiting.push(socket);

    socket.on('auth', data => this.auth(data));
    socket.on('action', data => this.action(data));

    socket.emit('ready-auth', {state:'ready-auth'});
  }

  auth(data) {
    var socket = this.getWaitingSocket(data.id);
    console.log("Auth!" + socket.id);
    console.log(this.state);

    for(var i=0, len=this.state.players.length; i<len; i++){
      if(this.state.players[i].name == data.name) {
        if(this.state.players[i].pass == data.pass) {
          socket.player_id = i;
          this.state.players[i].socket_id = socket.id;

          this.sockets.push(socket);

          socket.emit('auth-result', {
            auth: true,
            player_id: i,
            state: this.getClientState()
          });
        } else {
          socket.emit('auth-result', {
            auth: false
          });
        }

        break;
      }
    }

    this.removeWaitingSocket(data.id);
  }

  action(data) {
    var socket = this.getSocket(data.socket_id);
    var player = this.getPlayer(socket.player_id);

    // data.text = entities.encode(data.text);

    switch (player.mode) {
      case 'choosing-white':
        var card_index = player.whites.indexOf(data.text);
        console.log(data.text, card_index, player.whites);

        if(card_index != -1) {
          player.submitted = data.text;
          this.state.options.push(data.text);
          player.whites.splice(card_index, 1);
          player.whites.push(card_data_manager.getWhiteCards(1));
          player.mode = 'waiting-players';

          //Check if we can switch to czar choosing
          var done=0;

          for(var i=0; i<this.state.players.length; i++)
            if(this.state.players[i].mode == 'waiting-players')
              done++;

          if(done == this.state.players.length) {
            for(var i=0; i<this.state.players.length; i++) {
              if(this.state.czar == i)
                this.state.players[i].mode = 'choosing-winner';
              else
                this.state.players[i].mode = 'waiting-czar';
            }
          }

          for (var i=0; i<this.sockets.length; i++) {
            this.sockets[i].emit('new-state', this.getClientState());
          }
        }

        break;

      case 'choosing-winner':
        var card_index = this.state.options.indexOf(data.text);

        if(card_index != -1) {
          //Give player score
          for(var i=0; i<this.state.players.length; i++) {
            if(this.state.players[i].submitted == data.text)
              this.state.players[i].score ++;

            this.state.players[i].submitted = undefined;
          }

          //Reset options
          this.state.last_round = {black: this.state.black, white: data.text};
          this.state.black = card_data_manager.getBlackCard();
          this.state.options = [];
          this.state.czar ++;
          if(this.state.czar == this.state.players.length)
            this.state.czar = 0;

          //Reset players
          for(var i=0; i<this.state.players.length; i++) {
            if(this.state.czar == i)
              this.state.players[i].mode = 'waiting-players';
            else
              this.state.players[i].mode = 'choosing-white';
          }

          for (var i=0; i<this.sockets.length; i++) {
            this.sockets[i].emit('new-state', this.getClientState());
          }
        }

        break;
    }

    state_storage.setState(this.state);

    // socket.emit('new-state', {
    //   state: this.getClientState()
    // });
  }

  getClientState() {
    var players = [];

    for(var i=0, len=this.state.players.length; i<len; i++) {
      players.push(Object.assign({}, this.state.players[i]));
      delete players[i].pass;

      if(players[i].submitted)
        players[i].submitted = true;
    }

    return {
      players: players,
      czar: this.state.czar,
      black: this.state.black,
      last_round: this.state.last_round,
      options: this.state.options
    }
  }

  getPlayer(id) {
    return this.state.players[id];
  }

  getSocket(id) {
    if(id.charAt(0) != '/')
      id = '/#' + id;

    if(this.sockets.length > 0) {
      for(var i=0, len=this.sockets.length; i<len; i++) {
        if(this.sockets[i].id == id)
          return this.sockets[i];
      }
    }

    console.log("Get Socket Error!");
  }

  getWaitingSocket(id) {
    if(id.charAt(0) != '/')
      id = '/#' + id;

    if(this.waiting.length > 0) {
      for(var i=0, len=this.waiting.length; i<len; i++) {
        if(this.waiting[i].id == id)
          return this.waiting[i];
      }
    }

    console.log("Get Waiting Socket Error!");
  }

  removeWaitingSocket(id) {
    if(id.charAt(0) != '/')
      id = '/#' + id;

    for(var i=0, len=this.waiting.length; i<len; i++) {
      if(this.waiting[i].id == id) {
        return this.waiting.splice(i, 1);
      }
    }

    console.log("Remove Waiting Socket Error!");
  }
}

var port = (process.env.PORT || 8000)

var http    = require('http');
var express = require('express');
var app     = express();
app.use(express.static('public'));

var server  = http.createServer(app);
server.listen(port);
var io      = require('socket.io').listen(server);

var client;

if(process.env.REDIS_URL) {
  client = require('redis').createClient(process.env.REDIS_URL);
  console.log("Redis connected");
}

var cards   = require('./data.js').cards;

var card_data_manager;
var state_storage;
var room;

card_data_manager = new CardDataManager();
state_storage     = new StateStorage(client);
room              = new Room();

io.on('connect', function(socket){
  room.connect(socket);
});

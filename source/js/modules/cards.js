export class Card {
  constructor(manager) {
    this.manager = manager
    this.id = manager.index_id ++;
  }

  addTo(elem) {
    elem.appendChild(this.element);
    return this;
  }

  remove() {
    this.element.parentElement.removeChild(this.element);
  }

  getNum() {
    return parseInt(this.element.getAttribute('sort-num'));
  }

  setNum(num) {
    this.element.setAttribute('sort-num', num);
  }
}

export class WhiteCard extends Card {
  constructor(manager, args) {
    super(manager);
    this.str = args[0];

    this.element    = document.createElement("div");
    this.inner_div  = this.element.appendChild(document.createElement("div"));
    this.text       = this.inner_div.appendChild(document.createElement("span"))
    this.text.innerHTML = this.str;

    this.controls   = this.inner_div.appendChild(document.createElement("div"));
    this.up         = this.controls.appendChild(document.createElement("div"));
    this.num        = this.controls.appendChild(document.createElement("div"));
    this.down       = this.controls.appendChild(document.createElement("div"));

    this.up.innerHTML   = '<i class="fa fa-chevron-up" aria-hidden="true"></i>';
    this.down.innerHTML = '<i class="fa fa-chevron-down" aria-hidden="true"></i>';
    this.num.innerHTML  = '0';

    this.element.className    = "white-card";
    this.inner_div.className  = "card-inner";
    this.controls.className   = "white-card-controls";
    this.up.className         = "white-card-up";
    this.num.className        = "white-card-num";
    this.down.className       = "white-card-down";

    this.setNum(0);
  }

  getText() {
    return this.str;
  }

  setText(str) {
    this.str = str;
    this.text.innerHTML = str;
  }

  setNum(num) {
    super.setNum(num);

    window.setTimeout(() => this.animateNum(), 50);

    this.num.innerHTML = num;
  }

  animateNum() {
    if(this.num.style.transform === "")
      this.num.style.transform = "rotate3d(1,0,0,360deg)";
    else
      this.num.style.transform = "";
  }

  click(e) {
    if(e.target.className.indexOf("up") != -1){
      this.setNum(this.getNum()+1);
      e.preventDefault();
    }

    if(e.target.className.indexOf("down") != -1){
      this.setNum(this.getNum()-1);
      e.preventDefault()
    }

    if(e.target.className.indexOf("num") != -1){
      if(confirm("Do you really want to choose: " + this.getText())) {
        this.manager.action({
          card: this,
          type: 'submit-white',
          text: this.getText()
        })
      }
    }

    return this;
  }
}

export class BlackCard extends Card {
  constructor(manager, args) {
    super(manager);
    var str = args[0];

    this.element    = document.createElement("div");
    this.inner_div  = this.element.appendChild(document.createElement("div"));
    this.text       = this.inner_div.appendChild(document.createElement("span"))
    this.text.innerHTML = str.replace(/_/g, "_____");

    this.element.className    = "black-card";
    this.inner_div.className  = "card-inner";

    this.setNum(1000);
  }
}

export class PlayersCard extends Card {
  constructor(manager, args) {
    super(manager);

    var player_arr = args[0];
    var czar_id    = args[1];

    this.element    = document.createElement("div");
    this.inner_div  = this.element.appendChild(document.createElement("div"));

    this.players = [];

    for(var i=0,len=player_arr.length; i<len; i++) {
      this.players[i] = this.inner_div.appendChild(document.createElement("div"));
      this.players[i].innerHTML = (i == czar_id ? '<i class="fa fa-hand-rock-o" aria-hidden="true"></i> ' : '<i class="fa fa-hand-rock-o invisible" aria-hidden="true"></i> ') + player_arr[i].name + "<span class='name-right'>[" + player_arr[i].score + "](" + (player_arr[i].submitted ? '<i class="fa fa-check" aria-hidden="true"></i> ' : '<i class="fa fa-check invisible" aria-hidden="true"></i> ') + ")</span>";
    }

    this.element.className    = "special-card";
    this.inner_div.className  = "card-inner";

    this.setNum(2000);
  }

  fixLen(str, num) {
    while(str.length < num)
      str += " ";

    return str;
  }
}

export class LastRoundCard extends Card {
  constructor(manager, args) {
    super(manager);

    var last_result = args[0];

    this.element    = document.createElement("div");
    this.inner_div  = this.element.appendChild(document.createElement("div"));

    this.last_black = this.inner_div.appendChild(document.createElement("div"));
    this.last_white = this.inner_div.appendChild(document.createElement("div"));

    this.last_black.innerHTML = last_result.black.replace(/_/g, "_____");
    this.last_white.innerHTML = last_result.white;

    this.element.className    = "special-card";
    this.inner_div.className  = "card-inner";
    this.last_black.className = 'last-black';
    this.last_white.className = 'last-white';

    this.setNum(3000);
  }
}

export class InfoCard extends Card {
  constructor(manager, args) {
    super(manager);

    var text = args[0];

    this.element    = document.createElement("div");
    this.inner_div  = this.element.appendChild(document.createElement("div"));

    this.inner_div.innerHTML = text;

    this.element.className    = "special-card";
    this.inner_div.className  = "card-inner";

    this.setNum(100);
  }
}

export class CardManager {
  constructor(container, action) {
    this.index_id = 0;
    this.deck = [];
    console.log(container);
    this.container = container;
    this.container.addEventListener("mouseup", this.click.bind(this));
    this.action = action;
    // this.container.addEventListener("touchend", this.click.bind(this));
  }

  click(e) {
    for(var i=0; i<this.deck.length; i++){
      if(this.deck[i].element.contains(e.target)){
        if(this.deck[i].click !== undefined)
          this.deck[i].click(e);

        this.sort();

        break;
      }
    }
  }

  addCard(type, ...args){
      return this.deck[this.deck.push(new type(this, args))-1].addTo(this.container);
  }

  removeCard(card){
    for(var i=0; i<this.deck.length; i++){
      if(this.deck[i].id == card.id){
        this.deck.splice(i, 1);
        return;
      }
    }
  }

  sort(){
    for(var i=0; i<this.deck.length; i++){
      this.deck[i].remove();
    }

    this.deck.sort(function(a, b){
      return -(a.getNum() - b.getNum());
    });

    for(var i=0; i<this.deck.length; i++){
      this.deck[i].addTo(this.container);
    }
  }
}

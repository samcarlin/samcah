//Nothing for now
// console.log(window.location.href);

var socket;

import {WhiteCard, BlackCard, PlayersCard, LastRoundCard, InfoCard, CardManager} from './modules/cards.js';

//Variables
var name = '';
var pass = '';
var card_container;
var card_manager;
var state;
var id;
var whites = [];
var blacks = [];

window.onload = function(){
  socket = new io();
  //Activate login prompts

  if(!localStorage['name']) {
    localStorage['name'] = window.prompt('Enter your name:');
    localStorage['pass'] = window.prompt('Enter your password:');
  }

  // TODO Stop sending plaintext password

  socket.on('ready-auth', SendAuth);
  socket.on('auth-result', AuthResult);
  socket.on('new-state', NewState);
}

function SendAuth() {
  console.log("Authenticating");
  socket.emit('auth', {
    name: localStorage['name'],
    pass: localStorage['pass'],
    id: socket.id
  });
}

function AuthResult(data) {
  console.log(data.state)

  state = data.state;
  id    = data.player_id;

  if(data.auth) {
    GenerateCards();
  } else {
    card_container = document.getElementById("card-container");
    card_manager = new CardManager(card_container, Action);

    card_manager.addCard(WhiteCard, "Sorry you entered invalid credentials, refresh to try again!");
    delete localStorage['name'];
    delete localStorage['pass'];
  }

  Resize();
  window.addEventListener("resize", Resize);
}

function Action(action_obj) {
  // console.log(action_obj);
  socket.emit('action', {
    socket_id: socket.id,
    text: action_obj.text,
    type: action_obj.type
  });
}

function NewState(data) {
  state = data;

  GenerateCards();
}

function GenerateCards() {
  if(card_container != undefined) {
    card_container.innerHTML = '';
  } else {
    card_container = document.getElementById("card-container");
  }

  card_manager = new CardManager(card_container, Action);

  switch(state.players[id].mode) {
    case 'choosing-white':
      card_manager.addCard(LastRoundCard, state.last_round, state.czar);
      card_manager.addCard(PlayersCard, state.players, state.czar);
      card_manager.addCard(BlackCard, state.black);
      card_manager.addCard(InfoCard, "Submit a card!");
      for(var i=0; i<state.players[id].whites.length; i++)
        card_manager.addCard(WhiteCard, state.players[id].whites[i]);
      break;

    case 'choosing-winner':
      card_manager.addCard(LastRoundCard, state.last_round, state.czar);
      card_manager.addCard(PlayersCard, state.players, state.czar);
      card_manager.addCard(BlackCard, state.black);
      card_manager.addCard(InfoCard, "You are choosing a winner!");
      for(var i=0; i<state.options.length; i++)
        card_manager.addCard(WhiteCard, state.options[i]);
      break;

    case 'waiting-players':
      card_manager.addCard(LastRoundCard, state.last_round, state.czar);
      card_manager.addCard(PlayersCard, state.players, state.czar);
      card_manager.addCard(BlackCard, state.black);
      card_manager.addCard(InfoCard, "Waiting for other players to submit");
      break;

    case 'waiting-czar':
      card_manager.addCard(LastRoundCard, state.last_round, state.czar);
      card_manager.addCard(PlayersCard, state.players, state.czar);
      card_manager.addCard(BlackCard, state.black);
      card_manager.addCard(InfoCard, "Waiting for the czar to choose");
      break;
  }
}

function Resize(e){
  var content = document.getElementById("content");
  var nav     = document.getElementById("nav");

  if(true){
    content.style.height = window.innerHeight-50 + "px";
    content.style.top = "50px;"
    nav.style.height = "50px";
  } else {
    content.style.height = window.innerHeight + "px";
    content.style.top = "0px;"
    nav.style.height = "0px";
  }
}
